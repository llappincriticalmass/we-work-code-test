# README #

* Please clone repo to folder
* mvn clean install
* cd target
* java -jar code-test-1.0-SNAPSHOT.jar {searchTerm}
* example: java -jar code-test-1.0-SNAPSHOT.jar the

### What is this repository for? ###

* We Work Concurrent Web Search Test
* v1.0
* By: Luke Lappin
* 2/20/2018

### Assumptions ###

* This jar will take one search term and match against a list of URL's concurrently 
* If the search term exists in the text, the URL will be written to a file results.txt
* The regex I used matches the whole word
* If an non HTTP success code is found, a stack trace will be written to the console