package com.wework;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * We Work Website Search
 * 02/19/2017
 * Luke Lappin
 */

public class App {

	// Concurrently search list of websites for search term.
	public static void main(String[] args) throws InterruptedException, MalformedURLException {

		String search = args[0];
		Map<String, String> map = new ConcurrentHashMap<String, String>();
		System.setProperty("http.maxConnections", "20");

		try {

			// Get synchronized list of urls to parse
			List<String> urlList = parseUrlList("https://s3.amazonaws.com/fieldlens-public/urls.txt");

			// For each URL start future thread to search
			for (final String url : urlList) {

				CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {

					return readInputStreamToString("http://www." + url);

				});
				String result = future.get();
				// Regex matches using case insensitive and using word boundaries
				if (result != null && result.length() > 0) {

					String regex = "\\b" + search + "\\b";

					Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
					Matcher matcher = pattern.matcher(result.toLowerCase());
					if (matcher.find()) {
						map.put(url, "true");
						System.out.println("URL:" + url + " TRUE");
					} else {
						System.out.println("URL:" + url + " FALSE");
					}
				}
			}

			writeResults(map);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Parse initial URL into synchronized list of URL's to search
	private static List<String> parseUrlList(String url) throws MalformedURLException, IOException {

		String urlResponse = readInputStreamToString(url);
		List<String> result = Arrays.asList(urlResponse.split("\\s*,\\s*"));
		List<String> synchronizedList = new ArrayList<String>();

		// Filter out non-urls. Refactor with better regex.
		for (String s : result) {
			boolean urlOk = false;
			if (s.contains(".")) {
				s = s.replaceAll("\"", "");
				s = s.replaceAll("////", "");
				if (!s.matches(".*\\d+.*")) {
					urlOk = true;
				}
			}
			if (urlOk) {
				synchronizedList.add(s);
			}
		}

		return Collections.synchronizedList(synchronizedList);

	}

	// Get HTTP URL stream Old School
	private static String readInputStreamToString(String url) {

		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) (new URL(url)).openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String result = null;
		StringBuffer sb = new StringBuffer();
		InputStream is = null;

		try {
			is = new BufferedInputStream(connection.getInputStream());
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String inputLine = "";
			while ((inputLine = br.readLine()) != null) {
				sb.append(inputLine);
			}
			result = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	// Write map values that match search
	private static void writeResults(Map<String, String> map) {

		FileWriter writer;
		try {
			writer = new FileWriter("./results.txt");
			for (String key : map.keySet()) {
				writer.write(key);
				writer.write(System.getProperty("line.separator"));
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
